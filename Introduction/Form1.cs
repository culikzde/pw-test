﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Introduction
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.Text = "Je zde mys";
            button1.ForeColor = Color.Red;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.Text = "Uz odesla";
            button1.ForeColor = Color.Blue;

        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            // button1.Left += 10;
        }

        public void send (string text)
        {
            outputBox.AppendText(text + "\r\n");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool check = checkBox1.Checked;
            int num = (int) numericUpDown1.Value;
            string txt = textBox1.Text;

            send ("checkbox: " + check);
            send ("number: " + num);
            send ("text: " + txt);
            send ("");
        }
    }
}
